// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	console.log('Cookies: ', req.cookies)
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/session", function(req, res, next) {
    console.log('Cookies: ', req.cookies)
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

//Pour les tests d'insert et d'update, si il entre dans le bon bloc
app.get("/supprimeSession", function(req, res, next) {
	db.run('DELETE FROM sessions;');
	res.json({'Finis':true});
});

app.post("/logOut", function(req, res, next) {
	var cookie = req.cookies['cleSession'];
	db.run("DELETE FROM sessions WHERE token= '"+cookie+"';");
	db.get("SELECT token FROM sessions WHERE token = '"+cookie+"'", function(err, data) {
		if(typeof data !== 'undefined'){
			res.json({'deconnecte':false});
		}else{
			res.clearCookie('cleSession');
			res.json({'deconnecte':true});
		}
	});

});

app.post("/alreadyLogin", function(req, res, next) {
	var cook = req.cookies['cleSession'];
	db.get("SELECT token FROM sessions WHERE token = '"+cook+"'", function(err, data) {
		if(typeof data !== 'undefined'){
			res.json({'statut' : 'false'});
		}else{
			res.json({'statut' : 'true'});
		}
	});

});

app.post("/login", function(req, res, next) {
	var user = req.body.user;
	var psd = req.body.psd;
	var cook = req.cookies['cleSession'];
	if(cook == "" || cook == null){
		db.get("SELECT ident,password FROM users WHERE ident = ? AND password = ?",[req.body.user,req.body.psd], function(err, data) {
			if(typeof data !== 'undefined'){
				var ladate=new Date();
				var nDate = ladate.getDate()+""+ladate.getMonth()+""+ladate.getFullYear();
				var token = Math.random().toString(36).substr(2)+user+nDate;
				db.get("SELECT ident FROM sessions WHERE ident = '"+user+"'", function(err, data) {
					if(typeof data !== 'undefined'){
						db.run("UPDATE sessions SET token= '"+token+"' WHERE ident ='"+user+"'");
					}else{
						db.run("INSERT INTO sessions(ident,token) VALUES ('"+user+"','"+token+"')");
					}
				});

				res.cookie('cleSession', token, { maxAge: 60*60*24*1, httpOnly: true });

				res.json({'statut' : 'true'});
			}else{	
				res.json({'statut' : 'false'});
			}
		});
	}
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
