function creerCookie(nom, valeur, jours) {
	if (jours) {
		var date = new Date();
		date.setTime(date.getTime()+(jours*24*60*60*1000));
		var expire = "; expire="+date.toGMTString();
	}else{
	 var expire = "";
	}
	document.cookie = nom+"="+valeur+expire+"; path=/";
}
function getCookie(sName) {
        var cookContent = document.cookie, cookEnd, i, j;
        var sName = sName + "=";
 
        for (i=0, c=cookContent.length; i<c; i++) {
                j = i + sName.length;
                if (cookContent.substring(i, j) == sName) {
                        cookEnd = cookContent.indexOf(";", j);
                        if (cookEnd == -1) {
                                cookEnd = cookContent.length;
                        }
                        return decodeURIComponent(cookContent.substring(j, cookEnd));
                }
        }       
        return null;
}

function eraseCookie(nom) {
	creerCookie(nom,"",-1);
}

$(document).ready(function(){
	
	$.ajax({
		method: "POST",	
		url:"/alreadyLogin"
	})
	.done(function(msg){
		if(msg.statut != "true"){
			$("#error").addClass("display-none");
			$("#connect").removeClass("display-none");
			$("#divLogOut").addClass("display-none");
			$("#formCo").addClass("display-none");
		}
	});	

	$("#btnSend").on('click',function(event){
		event.preventDefault();
		$.ajax({
			method: "POST",	
			url:"/login",
			data:{user:$("#user").val(), psd:$("#psd").val()}
		})
		.done(function(msg){
			if(msg.statut != "true"){
				$("#error").removeClass("display-none");
				$("#connect").addClass("display-none");
				$("#divLogOut").addClass("display-none");
			}else{
				$("#error").addClass("display-none");
				$("#connect").removeClass("display-none");
				$("#divLogOut").addClass("display-none");
				$("#formCo").addClass("display-none");
			}
		});
	});

	$("#btnLogOut").on('click',function(event){
		event.preventDefault();
		var cki = getCookie('cleSession');
		$.ajax({
			method: "POST",	
			url:"/logOut"
		})
		.done(function(msg){
			if(msg.deconnecte != true){
				$("#error").addClass("display-none");
				$("#connect").addClass("display-none");
				$("#divLogOut").removeClass("display-none");
				$("#formCo").addClass("display-none");
			}else{
				eraseCookie("cleSession");
				$("#error").addClass("display-none");
				$("#connect").addClass("display-none");
				$("#divLogOut").addClass("display-none");
				$("#formCo").removeClass("display-none");
			}
		});
	});


});

